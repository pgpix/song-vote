<?php

/* App Core Class
 * Creates URL & loads core controller 
 * URL FORMAT - /controller/method/params
 */

 class Core {
    protected $currentController = 'Pages';
    protected $currentMethod = 'index';
    protected $params = [];

    public function __construct(){
        $url = $this->getUrl();

        // Look in controllers-directory for first value (the controller))
        if(isset($url[0])){
            if(file_exists('../app/controllers/' . ucwords( $url[0]) . '.php')) {
                $this->currentController = ucwords($url[0]);

                // Unset 0 Index to get the params easier with array_values($url)
                unset($url[0]);
            }else {
                redirect('pages/help');
            }
        }
        // Require the controller
        require_once '../app/controllers/' . $this->currentController . '.php';
        
        // Instantiate controller class
        $this->currentController = new $this->currentController;



        // Check for second part of url (the method)
        if(isset($url[1])){
            //Check if method exists in controller
            if(method_exists($this->currentController, $url[1])){
                $this->currentMethod = $url[1];
                // Unset 1 Index to get the params easier with array_values($url)
                unset($url[1]);
            } else {
                redirect('pages/help');
            }
        }

        $this->params = $url ? array_values($url) : [];

        // Call a callback
        call_user_func_array([$this->currentController, $this->currentMethod],  $this->params);

    }

    public function getUrl(){
        // print_r($_GET);
        // exit;
        if(isset($_GET['url'])){
            $url = rtrim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);
            // return an array with the url-parts
            return $url;
        };
    }
 } 