<?php
class Charts extends Controller {
    public function __construct(){
        if(isLoggedIn()){
            $this->interpretationModel = $this->model('Interpretation');
            $this->ratingModel = $this->model('Rating');
            $this->songModel = $this->model('Song');
            $this->userModel = $this->model('User');
            $this->bandModel = $this->model('Band');
            $this->urlModel = $this->model('Url');
        } else {
            redirect('users/login');
        }
    }

    public function index(){
        $interpretations = $this->interpretationModel->getAllInterpretations();
        $likes = $this->ratingModel->getLikes();
        $data = [
            'interpretations' => $interpretations,
            'likes' => $likes
        ];

        $this->view('charts/index',$data );
    }

    // Alias for a nicer URL
    public function votesongs() { 
        return call_user_func( 'Charts::showDragDropList');
    }

    // Fetch Interpretations for voting by drag 'n drop
    public function showDragDropList(){
        // $interpretations = $this->interpretationModel->getUserInterpretations();
        $votedInterpretations = $this->interpretationModel->getVoted();
        $unvotedInterpretations = $this->interpretationModel->getUnVoted();
        $data = [
            'votedInter' => $votedInterpretations,
            'unvotedInter' => $unvotedInterpretations
        ];

        $this->view('charts/votesongs',$data );
    }


    public function add(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
                'title' => trim($_POST['title']),
                'band_name' => trim($_POST['band_name']),
                'user_id' => $_SESSION['user_id'],
                'title_err' => ''
            ];

            // Set error message if input empty
            if(empty($data['title'])){
                $data['title_err'] = 'Please enter title';
            }

            // Make sure there is no input error
            if(empty($data['title_err'])){
                $songInDB = $this->songModel->getSongByTitle($data['title']);
                if($songInDB){
                    $songId = $songInDB->song_id;
                } else {
                    $songId = $this->songModel->add($data);
                }

                $bandInDb = false;
                $bandId = null;
                if(!empty($data['band_name'])){
                    $bandInDb = $this->bandModel->getBandByName($data['band_name']);
                    if($bandInDb){
                        // dump('then');
                        $bandId = $bandInDb->band_id;
                    } else {
                        // dump('else');
                        $bandId = $this->bandModel->add($data);
                    } 
                }

                
                if($this->interpretationModel->interpretationExists($songId, $bandId)){
                    // dump('THEN songId: '.$songId.' bandId: '.$bandId);
                    flash('song_message', $data['title'].' played by '.$data['band_name'].' is already registered!','alert alert-warning');
                    $this->view('charts/add', $data);
                } else {
                    $inter_id = $this->interpretationModel->add($songId, $bandId);
                    flash('song_message', $data['title'].' played by '.$data['band_name'].' is successfully registered!');
                    redirect('charts/votesongs');
                }
            } else {
                // ...no text in song input -> reload view with errors
                $this->view('charts/add', $data);
            }
        } else {
            // Get request -> load empty form-view
            $data = [
                'title' => '',
                'band_name' => '',
                'title_err' => ''
            ];
            $this->view('charts/add', $data);
        }
    }

    // Catch data from ajax (drag 'n drop list)
    public function ratesong(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $score_list = $_POST['data'];
            foreach($score_list as $key => $obj){
                // error_log($obj['interid'].' - '.$obj['score']);
                $data = [
                    'inter_id' => $obj['interid'], 
                    'rating' => $obj['score']
                ];
                $this->ratingModel->putRating($data);
            }
        }  
    }

    public function details($interId){
        $interpretation = $this->interpretationModel->getInterpretationById($interId);
        $songId = $interpretation->song_id;
        $furtherInter = $this->interpretationModel->getFurtherInterpretationsBySongId($songId,$interId);
        $ratings = $this->ratingModel->getRatingsByInterId($interId);
        $user = $this->userModel->getUserNameById($interpretation->added_by);
        $urls = $this->urlModel->getUrlsByInterId($interId);
        $data = [
        'interpretation' => $interpretation,
        'furtherinter' => $furtherInter,
        'ratings' => $ratings,
        'user' => $user,
        'urls' => $urls,
        ];

        $this->view('charts/details', $data);
    }

    public function updateSong(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
                'title' => trim($_POST['title']),
                'song_id' => $_POST['song_id']
            ];
            if($this->songModel->updateSong($data)){
                echo 'success';
            } else {
                echo 'failure';
            }
        } else {
            console.log("This wasn't a POST-REQUEST");
        }
    }
    
    // Catch data from ajax
    public function addUrl(){   
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            error_log($_POST['url']);
            $trimed_url = trim($_POST['url']);
            
            // filter_var is useless since 
            // FILTER_FLAG_SCHEME_REQUIRED and FILTER_FLAG_HOST_REQUIRED is deprecated
            $url_is_valid = false;
            if ( preg_match_all('/https?:\/\/[\w.]+[\w\/]*[\w.]*\??[\w=&\+\%]*/is',$trimed_url) === 1 ) {
                $url_is_valid = true;
            } else {
                echo 'URL is invalid.';
                exit;
            }
            $data = [
                'url' => trim($_POST['url']),
                'url_title' => trim($_POST['url_title']),
                'inter_id' => $_POST['inter_id']
            ];
            if($this->urlModel->add($data)){
                echo 'success';
            } else {
                echo 'Link could not  be stored.';
            }
        } else {
            console.log("This wasn't a POST-REQUEST");
        }
    }

    private function getSongIfOwner($songId){
        $song = $this->songModel->getSongById($songId);
        if($song->added_by != $_SESSION['user_id']){
            redirect($_SERVER['HTTP_REFERER']);
            exit;
        } else {
            return $song;
        }
    }

    // Catch data from ajax
    // public function delete($id){
    //     // Check for owner
    //     $song = $this->getSongIfOwner($id);
        
    //     if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //         if($this->songModel->deleteSong($song->song_id)){
    //             flash('song_message', 'Song successfully removed');
    //             redirect($_SERVER['HTTP_REFERER']);
    //         } else {
    //             die('Somthing went wrong');
    //         }
    //     } else {
    //         redirect($_SERVER['HTTP_REFERER']);
    //     }
    // }

    // Catch data from ajax
    public function toggleLikeStar(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            if($this->ratingModel->toggleLikeStar($_POST['interid'])){
                $response[] = 'success';
                $response[] = $this->ratingModel->getRatingsByInterId($_POST['interid']);
                echo json_encode($response, JSON_FORCE_OBJECT);
                // echo 'success';
            } else {
                echo 'fail';
            }
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    // Catch data from ajax
    public function getLikeStar(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $ratings = $this->ratingModel->getRatingsByInterId($_POST['interid']);
            if($ratings){
                $response[] = 'success';
                $response[] = $ratings;
                echo json_encode($response, JSON_FORCE_OBJECT);
            }
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

}