<?php
    class Users extends Controller {
        public function __construct()
        {
            $this->userModel = $this->model('User');
        }

        public function register(){
            if(NO_NEW_USERS_ALLOWED){
                redirect('about');
            }
            // Check for POST
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                // Process the form
                // Sanitize POST data
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                // Init 
                $data =[
                    'name' => trim($_POST['name']),
                    'email' => trim($_POST['email']),
                    'password' => trim($_POST['password']),
                    'confirm_password' => trim($_POST['confirm_password']),
                    'name_err' => '',
                    'email_err' => '',
                    'password_err' => '',
                    'confirm_password_err' => ''
                ];

                // Validate if email already in use
                if(empty($data['email'])){
                    $data['email_err'] = 'Please enter email';
                } else {
                    if($this->userModel->findUserByEmail($data['email'])){
                        $data['email_err'] = 'Email is already registered!';
                    }
                }

                // Validate name
                if(empty($data['name'])){
                    $data['name_err'] = 'Please enter name';
                }

                // Validate password
                if(empty($data['password'])){
                    $data['password_err'] = 'Please enter password';
                } elseif(strlen($data['password']) < 6){
                    $data['password_err'] = 'Password must be at least 6 characters';
                }

                // Validate confirm_password
                if(empty($data['confirm_password'])){
                    $data['confirm_password_err'] = 'Please confirm password';
                } elseif($data['confirm_password'] != $data['password']){
                    $data['confirm_password_err'] = 'Passwords do not match';
                }

                // Make shure errors are empty
                if(empty($data['email_err']) && empty($data['name_err']) && empty($data['password_err']) && empty($data['confirm_password_err'])){
                    // Validated
                    $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
                    // Register user
                    if($this->userModel->register($data)){
                        flash('register_success', 'You are successfully registered. Please log in.');
                        redirect('users/login');
                    } else {
                        die('Something went wrong with the registration') ;
                    }
                } else {
                    // Load view with errors
                    $this->view('/users/register', $data);
                }
            } else {
                // Form is referred by a link
                // Init 
                $data =[
                    'name' => '',
                    'email' => '',
                    'password' => '',
                    'confirm_password' => '',
                    'name_err' => '',
                    'email_err' => '',
                    'password_err' => '',
                    'confirm_password_err' => ''
                ];
                // Load view
                $this->view('users/register', $data);
            }
        }

        public function login(){
            // Check for POST
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                // Process the form
                // Sanitize POST data
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                // Init 
                $data =[
                    'email' => trim($_POST['email']),
                    'password' => trim($_POST['password']),
                    'email_err' => '',
                    'password_err' => '',
                ];
                // Validate email
                if(empty($data['email'])){
                    $data['email_err'] = 'Please enter email';
                }

                // Validate password
                if(empty($data['password'])){
                    $data['password_err'] = 'Please enter password';
                } elseif(strlen($data['password']) < 6){
                    $data['password_err'] = 'Password must be at least 6 characters';
                }

                $loginErrorMessage = 'User or password are incorrect.';

                // Check if user/email exists
                if($this->userModel->findUserByEmail($data['email'])){
                    //ok
                } else {
                    $data['password_err'] = $loginErrorMessage;
                }


                // Make shure errors are empty
                if(empty($data['email_err']) && empty($data['password_err'])){
                    // $this->userModel->login checks if user and password are valid,
                    // and returns user-record or false 
                    $loggedInUser = $this->userModel->login($data['email'], $data['password']);
                    if($loggedInUser){
                        // Create session
                        $this->createUserSession($loggedInUser);
                    } else {
                        $data['password_err'] = $loginErrorMessage;
                        // redirect to login form with all data
                        $this->view('users/login', $data);
                    }

                } else {
                    // Load view with errors
                    $this->view('/users/login', $data);
                }

            } else {
                // form loaded via link so send an empty form
                // Init 
                $data =[
                    'email' => '',
                    'password' => '',
                    'email_err' => '',
                    'password_err' => ''
                ];
                // Load view
                $this->view('users/login', $data);
            }
        }
        
        public function createUserSession($user){
            $_SESSION['user_id'] = $user->user_id;
            $_SESSION['user_email'] = $user->email;
            $_SESSION['user_name'] = $user->name;
            $this->loglog($_SESSION['user_name'], 'IN');
            redirect('charts/votesongs');
        }

        public function logout(){
            $this->loglog($_SESSION['user_name'], 'OUT');
            unset($_SESSION['user_id']);
            unset($_SESSION['user_email']);
            unset($_SESSION['user_name']);
            session_destroy();
            redirect('users/login');
        }

        public function index(){
            if (isLoggedIn()) {
                $users = $this->userModel->getAllusers();
                $data = [
                    'users' => $users
                ];
                $this->view('users/index', $data);
            } else {
                redirect('users/login');
            }
        }

        private function loglog($username, $direction){
            if(defined('LOG_ACCESS_FILE')){
                file_put_contents(LOG_ACCESS_FILE,date('Y.m.d \@ G:i:s').' : '.$username.' logged '.$direction.PHP_EOL, FILE_APPEND | LOCK_EX);
            }
        }
    }