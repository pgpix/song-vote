<?php
class Bands extends Controller {
    public function __construct(){
        if(isLoggedIn()){
            $this->bandModel = $this->model('Band');
            $this->songModel = $this->model('Song');
            // $this->userModel = $this->model('User');
        } else {
            redirect('users/login');
        }
    }

    public function index(){
        $bands = $this->bandModel->getAllBands();
        $data = [
            'bands' => $bands
        ];
        $this->view('bands/index',$data );
    }
    
    public function details($bandId){
        $bandSongs = $this->songModel->getSongsByBandId($bandId);
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
                'band_id' => $bandId,
                'band_name' => trim($_POST['band_name']),
                'band_name_err' => '',
                'band_songs' => $bandSongs
            ];

            // Validate data 
            if(empty($data['band_name'])){
                $data['band_name_err'] = 'Please enter bandname';
            }

            // Make sure no error
            if(empty($data['band_name_err'])){
                // Validated
                if($this->bandModel->updateBandname($data)){
                    flash('post_message','Band succefully updated');
                    redirect('bands/details/'.$bandId);
                } else {
                    die('Something went wrong');
                }
            } else {
                // load view with error
                $this->view('bands/details', $data);
            }

        } else {
            $band = $this->bandModel->getBandById($bandId);
            $data = [
                'band_id' => $bandId,
                'band_name' => $band->band_name,
                'band_name_err' => '',
                'band_songs' => $bandSongs
            ];
        }

        $this->view('bands/details',$data );
    }

}