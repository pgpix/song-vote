<?php
class Pages extends Controller {
    public function __construct(){
        $this->interpretationModel = $this->model('Interpretation');
    }

    public function index(){
        $interpretations = $this->interpretationModel->getAllInterpretations();
        $data = [
            'title' => SITENAME,
            'description' => DESCRIPTION,
            'interpretations' => $interpretations
        ];
        $this->view('pages/index',$data );
    }

    public function about(){
        $data = [
            'title' => 'Why I wrote SONGVOTE.',
            'description' => 'Vote for democracy!'
        ];
        $this->view('pages/about',$data );
    }

    public function help(){
        $data = [
            'title' => 'SONGVOTE HELP',
            'description' => 'How to vote and filter your favorite songs'
        ];
        $this->view('pages/help',$data );
    }
}