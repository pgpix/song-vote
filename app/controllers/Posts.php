<?php
class Posts extends Controller {
    public function __construct(){
        if(!isLoggedIn()){
            redirect('users/login');
        } else {
            $this->userModel = $this->model('User');
            $this->postModel = $this->model('Post');
        }
    }

    public function index(){
        $posts = $this->postModel->getPosts();
        $data = [
            'posts' => $posts
        ];

        $this->view('posts/index',$data );
    }

    public function add(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
                'title' => trim($_POST['title']),
                'body' => trim($_POST['body']),
                'user_id' => $_SESSION['user_id'],
                'title_err' => '',
                'body_err' => ''
            ];

            // Validate data 
            if(empty($data['title'])){
                $data['title_err'] = 'Please enter title';
            }
            if(empty($data['body'])){
                $data['body_err'] = 'Please enter body text';
            }

            // Make sure no error
            if(empty($data['title_err']) && empty($data['body_err'])){
                // Validated
                if($this->postModel->addPost($data)){
                    flash('post_message','Post succefully added');
                    redirect('posts');
                } else {
                    die('Something went wrong');
                }
            } else {
                // load view with error
                $this->view('posts/add', $data);
            }

        } else {
            $data = [
                'title' => '',
                'body' => ''
            ];
        }

        $this->view('posts/add',$data );
    }

    public function edit($id){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
                'id' => $id,
                'title' => trim($_POST['title']),
                'body' => nl2br(trim($_POST['body'])),
                'user_id' => $_SESSION['user_id'],
                'title_err' => '',
                'body_err' => ''
            ];

            // Validate data 
            if(empty($data['title'])){
                $data['title_err'] = 'Please enter title';
            }
            if(empty($data['body'])){
                $data['body_err'] = 'Please enter body text';
            }

            // Make sure no error
            if(empty($data['title_err']) && empty($data['body_err'])){
                // Validated
                if($this->postModel->updatePost($data)){
                    flash('post_message','Post succefully updated');
                    redirect('posts');
                } else {
                    die('Something went wrong');
                }
            } else {
                // load view with error
                $this->view('posts/edit', $data);
            }

        } else {
            // Check for owner
            $post = $this->postModel->getPostById($id);
            if($post->user_id != $_SESSION['user_id']){
                redirect('posts');
            }
            $data = [
                'id' => $id,
                'title' => $post->title,
                'body' => $post->body
            ];
        }

        $this->view('posts/edit',$data );
    }


    public function show($id){
        $post = $this->postModel->getPostById($id);
        $user = $this->userModel->getUserById($post->user_id);
  
        $data = [
          'post' => $post,
          'user' => $user
        ];
        // var_dump($data);
        // die();
        $this->view('posts/show', $data);
    }

    public function delete($id){
        // Check for owner
        $post = $this->postModel->getPostById($id);
        if($post->user_id != $_SESSION['user_id']){
            redirect('posts');
        }
        
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($this->postModel->deletePost($id)){
                flash('post_message', 'Post successfully removed');
                redirect('posts');
            } else {
                die('Somthing went wrong');
            }
        } else {
            redirect('posts');
        }
    }
}