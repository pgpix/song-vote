<?php
// Load Config
require_once 'config/config.php';
require_once 'helpers/dev_helper.php';
require_once 'helpers/import_helper.php';
require_once 'helpers/url_helper.php';
require_once 'helpers/session_helper.php';
require_once 'helpers/view_helpers.php';

// Autoload Core Libraries
spl_autoload_register(function($className){
    require_once 'libraries/'. $className .'.php';
});