<?php
// $session_lifetime = 864000; // 10 days
// session_set_cookie_params(['SameSite' => 'strict', 'secure' => true, 'httponly' => false]);
// session_name('RALEF');
$host = getenv('HTTP_HOST');
$arr_cookie_options = array (
    'expires' => time() + 60*60*24*20,
    'path' => '/',
    'domain' => $host, // leading dot for compatibility or use subdomain
    'secure' => true,     // or false
    'httponly' => false,    // or false
    'samesite' => 'Strict' // None || Lax  || Strict
    );

session_start();
setcookie(session_name(),session_id(),$arr_cookie_options);

function flash($name = '', $message = '', $class='alert alert-success'){
    unset($_SESSION['test']);
    if(isset($name)){
        // name has to be set always
        if(isset($message) && empty($_SESSION[$name])){
            // Set the message for later use
            $_SESSION[$name] = $message;
            $_SESSION[$name . '_class'] = $class;
        } elseif(empty($message) && isset($_SESSION[$name])){
            // call the message in loginform
            $class = isset($_SESSION[$name. '_class']) ? $_SESSION[$name. '_class'] : '';
            echo '<div class="'.$class.'" id="msg-flash">'.$_SESSION[$name].'</div>';
            unset($_SESSION[$name]);
            unset($_SESSION[$name. '_class']);
        }
    }
}


function isLoggedIn(){
    if(isset($_SESSION['user_id'])){
        return true;
    } else {
        return false;
    }
}