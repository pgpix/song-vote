<?php
    function generateStarFilter($data){
        $users = [];
        foreach($data['likes'] as $liked ){
            $uname = $liked->name;
            $uid = $liked->user_id;
            if(!in_array($uid, $users)){
                $users[$uid] = $uname;
            }
        };
        echo '<div class="like-filter text-center" data-userid="ALL"><i class="fas fa-star text-gold"></i><p class="p-0 m-0">ALL</p></div>';
        asort($users);
        foreach($users as $uid => $usr){
            echo '<div role="button" class="like-filter text-center" data-userid="'.$uid.':"><i class="far fa-star text-gold"></i><p class="p-0 m-0">'.$usr. '</p></div>';
        }
        echo '<div class="like-filter text-center" data-userid="NOBODY"><i class="far fa-star text-dark"></i><p class="p-0 m-0">Nobody</p></div>';
    }


    function addStarToggle($inter){
        echo '<i class="far fa-star star-toggle pulser float-right text-dark-50 " data-interid="'.$inter->inter_id.'" data-cuid="'. $_SESSION['user_id'] .'"></i>';
    }
