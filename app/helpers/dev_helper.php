<?php

    function dumpex($obj){
        echo '<pre>';
        var_dump($obj);
        echo '</pre>';
        exit;
    }

    function dump($obj){
        echo '<pre>';
        var_dump($obj);
        echo '</pre>';
    }

    function logex($arr){
        foreach($arr as $obj){
            error_log(print_r($obj, true));
        };
    }