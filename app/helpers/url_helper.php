<?php
    function redirect($page){
        header('location: ' . URLROOT . '/' .$page);
    }

    function buildClassFromURL(){
        if(isset($_GET['url'])){
            $url_array = explode("/",$_GET['url']);
            $slug = end($url_array);
            if(is_numeric($slug)){
                $slug = prev($url_array);
            }
            echo ' class="' . $slug . '"';
        }
    }