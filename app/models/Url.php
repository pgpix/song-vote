<?php
    class Url {
        private $db;

        public function __construct(){
            $this->db = new Database;
        }

        // public function getAllBands(){
        //         $this->db->query('SELECT * FROM bands
        //                             ORDER BY band_name
        //                         ');
        //         $results = $this->db->resultSet();
        //     return $results;
        // }
        
        // public function getBandById($bandId){
        //     // $this->db->startTransaction();
        //     $this->db->query('SELECT * FROM bands WHERE band_id = (:band_id);');
        //     $this->db->bind(':band_id', $bandId);
        //     $bandAlreadyStored = $this->db->single();
        //     return $bandAlreadyStored;
        // }

        public function add($data){
            $this->db->query(
                'INSERT INTO urls (url, url_title, inter_id) 
                    VALUES (:url, :url_title, :inter_id)'
                );
            $this->db->bind(':url', $data['url']);
            $this->db->bind(':url_title', $data['url_title']);
            $this->db->bind(':inter_id', $data['inter_id']);
            return $this->db->execute();
        }

        public function getUrlsByInterId($interId){
            // dump($interId);
            $this->db->query(
                'SELECT url, url_title
                    FROM urls 
                    WHERE inter_id = :inter_id
                ');
            $this->db->bind(':inter_id', $interId);
            return $this->db->resultSet();
        }


        // public function updateBandname($data){
        //     $this->db->query('UPDATE bands SET band_name = :band_name WHERE band_id = :band_id');
        //     $this->db->bind(':band_id', $data['band_id']);
        //     $this->db->bind(':band_name', $data['band_name']);
        //     return $this->db->execute();
        // } 
    }