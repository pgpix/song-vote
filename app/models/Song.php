<?php
    class Song {
        private $db;
        private $defaultRating = 0;
        private $defaultFixed = 0;

        public function __construct(){
            $this->db = new Database;
        }

        public function getSongById($songId){
            $this->db->query(
                'SELECT s.title, s.song_id, s.added_by, b.band_name, r.rating score, r.user_id, r.liked
                FROM songs s
                LEFT JOIN interpretations i
                ON i.song_id = s.song_id
                LEFT JOIN bands b 
                ON i.band_id = b.band_id
                LEFT JOIN ratings r
                ON s.song_id = r.inter_id
                WHERE s.song_id = (:songId)
            ');
            $this->db->bind(':songId', $songId);
            return $this->db->single();
        }

        public function getSongByTitle($title){
            $this->db->query('SELECT * FROM songs WHERE title = (:title);');
            $this->db->bind(':title', $title);
            return $this->db->single();
        }

        public function getSongsByBandId($band_id){
            $this->db->query(
                'SELECT * 
                    FROM interpretations
                    LEFT JOIN songs USING (song_id)
                    WHERE band_id = (:band_id)
                    ORDER by title;
                ');
            $this->db->bind(':band_id', $band_id);
            return $this->db->resultSet();
        }

        public function add($data){
            $this->db->query('INSERT INTO songs (title, added_by) VALUES (:title, :user_id);');
            $this->db->bind(':title', $data['title']);
            $this->db->bind(':user_id', $_SESSION['user_id']);
            return $this->db->executeAndGetId();
        }



        public function updateSong($data){
            $this->db->query('UPDATE songs SET title = :title WHERE song_id = :song_id');
            $this->db->bind(':song_id', $data['song_id']);
            $this->db->bind(':title', $data['title']);
            return $this->db->execute();
        }


        // public function deleteSong($songId){
        //     if(empty($songId)){
        //         return('whitout song id all song would be deleted!');
        //     }
        //     $this->db->startTransaction();
        //         // Delete rating
        //         $this->db->query('DELETE FROM ratings WHERE inter_id = (:inter_id) ');
        //         $this->db->bind(':inter_id',$songId);
        //         $this->db->execute();

        //         // Delete interpretations
        //         $this->db->query('DELETE FROM interpretations WHERE song_id = (:song_id) ');
        //         $this->db->bind(':song_id',$songId);
        //         $this->db->execute();
                
        //         // Delete Song
        //         $this->db->query('DELETE FROM songs WHERE song_id = (:song_id) ');
        //         $this->db->bind(':song_id',$songId);
        //         $this->db->execute();
        //     return $this->db->endTransaction();
        // }
    }