<?php
    class Interpretation {
        private $db;
        private $defaultRating = 0;
        private $defaultLiked = 0;

        public function __construct(){
            $this->db = new Database;
        }

        public function getAllInterpretations(){
            $this->db->query(
                'SELECT title, inter_id, band_id, band_name, SUM(rating) score
                    FROM songs
                    LEFT JOIN interpretations USING (song_id)
                    LEFT JOIN bands USING (band_id)
                    LEFT JOIN ratings USING (inter_id)
                    GROUP BY inter_id
                    ORDER BY score DESC, song_id DESC
            ');
            return $this->db->resultSet();
        }

        public function cntInterpretations(){
            $this->db->query('SELECT count(*) as quantity FROM interpretations');
            return $this->db->single();
        }

        public function getVoted(){
            $this->db->query(
                'SELECT inter_id, song_id, title, band_name, rating, liked 
                    FROM interpretations
                    RIGHT JOIN ratings  USING (inter_id)
                    LEFT JOIN songs USING (song_id)
                    LEFT JOIN bands USING (band_id)
                    WHERE user_id = (:user_id)
                    ORDER BY rating DESC'
            );
            $this->db->bind(':user_id', $_SESSION['user_id']);
            return $this->db->resultSet();
        }

        public function getUnVoted(){
            $this->db->query(
                'SELECT inter_id, song_id, title, band_name
                    FROM interpretations
                    LEFT JOIN songs USING (song_id)
                    LEFT JOIN bands USING (band_id)
                    WHERE inter_id NOT IN (
                        SELECT DISTINCT inter_id
                        FROM ratings
                        WHERE user_id = (:user_id) 
                    )'
            );
            $this->db->bind(':user_id', $_SESSION['user_id']);
            return $this->db->resultSet();
        }

        // public function getUserInterpretations(){
        //     $this->db->query(
        //         'SELECT * FROM (
        //             SELECT 
                        // inter_id, 
                        // song_id, 
                        // title, 
                        // band_name, 
                        // rating AS score, 
                        // (   SELECT SUM(liked) 
                        //     FROM ratings 
                        //     WHERE inter_id = i.inter_id 
                        // ) AS liked
        //                 FROM interpretations i
        //                 LEFT JOIN ratings USING (inter_id)
        //                 LEFT JOIN songs USING (song_id)
        //                 LEFT JOIN bands USING (band_id)
        //                 WHERE user_id = (:user_id)
        //             UNION
        //             SELECT      
        //                 inter_id, 
        //                 song_id, 
        //                 title, 
        //                 band_name, 
        //                 rating AS score, 
        //                 (   SELECT SUM(liked) 
        //                     FROM ratings 
        //                     WHERE inter_id = i.inter_id 
        //                 ) AS liked
        //                 FROM interpretations i
        //                 LEFT JOIN ratings USING (inter_id)
        //                 LEFT JOIN songs USING (song_id)
        //                 LEFT JOIN bands USING (band_id)
        //                 WHERE user_id != (:user_id) OR rating_id IS NULL
        //         ) AS requiredAlias GROUP BY inter_id ORDER BY score DESC, inter_id DESC ;
        //     ');
        //     $this->db->bind(':user_id', $_SESSION['user_id']);
        //     return $this->db->resultSet();
        // }

        public function getInterpretationById($interID){
            $this->db->query(
                'SELECT s.title, s.song_id, i.added_by, u.name AS user_name, b.band_name, b.band_id, i.inter_id
                    FROM interpretations i
                    LEFT JOIN songs s USING (song_id)
                    LEFT JOIN bands b USING (band_id)
                    LEFT JOIN users u ON i.added_by = u.user_id
                    WHERE i.inter_id = (:interID)
            ');
            $this->db->bind(':interID', $interID);
            return $this->db->single();
        }

        public function getFurtherInterpretationsBySongId($songId,$interID){
            $this->db->query(
                'SELECT * 
                    FROM interpretations
                    LEFT JOIN bands b USING (band_id)
                    WHERE song_id = (:songId)
                    AND inter_id != (:interID)
            ');
            $this->db->bind(':songId', $songId);
            $this->db->bind(':interID', $interID);
            return $this->db->resultSet();
        }

        public function add($songId, $bandId){
            $this->db->query('INSERT INTO interpretations (song_id, band_id, added_by ) VALUES (:song_id, :band_id, :added_by)');
            $this->db->bind(':song_id', $songId);
            $this->db->bind(':band_id', $bandId);
            $this->db->bind(':added_by', $_SESSION['user_id']);
            return $this->db->executeAndGetId();
        }

        public function interpretationExists($songId, $bandId){
            $this->db->query('SELECT SQL_NO_CACHE * FROM interpretations WHERE song_id = (:song_id) AND (band_id = (:band_id) OR band_id IS NULL)');
            $this->db->bind(':song_id', $songId);
            $this->db->bind(':band_id', $bandId);
            $row = $this->db->single();

            if($this->db->rowCount() > 0){
                return true;
            } else {
                return false;
            }
        }


        // public function deleteSong($songId){
        //     if(empty($songId)){
        //         return('whitout song id all song would be deleted!');
        //     }
        //     $this->db->startTransaction();
        //         // Delete rating
        //         $this->db->query('DELETE FROM ratings WHERE inter_id = (:inter_id) ');
        //         $this->db->bind(':inter_id',$songId);
        //         $this->db->execute();

        //         // Delete interpretations
        //         $this->db->query('DELETE FROM interpretations WHERE song_id = (:song_id) ');
        //         $this->db->bind(':song_id',$songId);
        //         $this->db->execute();
                
        //         // Delete Song
        //         $this->db->query('DELETE FROM songs WHERE song_id = (:song_id) ');
        //         $this->db->bind(':song_id',$songId);
        //         $this->db->execute();
        //     return $this->db->endTransaction();
        // }
    }