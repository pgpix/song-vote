<?php

class User{
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function register($data){
        $this->db->query('INSERT INTO users (name, email, password) VALUES(:name, :email, :password)');
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', $data['password']);
        return $this->db->execute();
    }

    public function findUserByEmail($email){
        $this->db->query('SELECT * FROM users WHERE email = :email');
        $this->db->bind(':email', $email);
        $row = $this->db->single();

        if($this->db->rowCount() > 0){
            return true;
        } else {
            return false;
        }
    }

    /*  login checks if user and password is valid,
    *   unhashes the password
    *   and returns user-record or false 
    */
    public function login($email, $password){
        // Get the whole record (object, row) with the assosiated email 
        // to get the password
        $this->db->query('SELECT * FROM users WHERE email = :email');
        $this->db->bind(':email', $email);
        $row = $this->db->single();
        $hashed_password = $row->password;
        if(password_verify($password, $hashed_password)){
            return $row;
        } else {
            return false;
        }
    }

    public function getUserById($id){
            $this->db->query('SELECT * FROM users where user_id = :id');
            $this->db->bind(':id', $id);
            return $this->db->single();
    }

    public function getUserNameById($id){
            $this->db->query('SELECT name FROM users where user_id = :id');
            $this->db->bind(':id', $id);
            return $this->db->single();
    }

    public function getAllusers(){
        $this->db->query('SELECT * FROM users');
        return $this->db->resultSet();
    }
}