<?php
    class Rating {
        private $db;
        private $defaultRating = 0;
        private $defaultLiked = 0;

        public function __construct(){
            $this->db = new Database;
        }

        // getRatingsByInterId used for song detail view
        public function getRatingsByInterId($interId){
            $this->db->query(
                'SELECT DISTINCT name AS user_name, u.user_id, rating, liked
                    FROM users u
                    LEFT JOIN ratings r
                    ON u.user_id = r.user_id AND inter_id = (:inter_id) OR inter_id IS NULL
                    ORDER BY rating DESC
            ');
            $this->db->bind(':inter_id', $interId);
            return $this->db->resultSet();
        }

        public function getLikes(){
            $this->db->query(
                'SELECT inter_id, user_id, name, liked
                    FROM ratings 
                    LEFT JOIN users USING (user_id)
                    ORDER by inter_id
                ');
            return $this->db->resultSet();
        }

        public function putRating($data){
            // error_log(print_r($data, TRUE));
            $this->db->query('INSERT INTO ratings (user_id, inter_id, rating, liked) 
                                     VALUES (:user_id, :inter_id, :rating, :liked) 
                                     ON DUPLICATE KEY 
                                     UPDATE rating = (:rating);');
            $this->db->bind(':user_id', $_SESSION['user_id']);
            $this->db->bind(':inter_id', $data['inter_id']);
            $this->db->bind(':rating', $data['rating']);
            $this->db->bind(':liked', $this->defaultLiked);
            return $this->db->execute();
        }
        
        public function toggleLikeStar($interId){
            $this->db->query(
                'INSERT INTO ratings (user_id, inter_id, rating, liked) 
                        VALUES (:user_id, :inter_id, :rating, :liked) 
                        ON DUPLICATE KEY 
                        UPDATE liked = 1 - liked;');
            $this->db->bind(':user_id', $_SESSION['user_id']);
            $this->db->bind(':inter_id', $interId);
            $this->db->bind(':rating', 0);
            $this->db->bind(':liked', 1);
            return $this->db->execute();
        }
    }