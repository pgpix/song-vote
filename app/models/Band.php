<?php
    class Band {
        private $db;
        private $defaultRating = 0;
        private $defaultVeto = 0;

        public function __construct(){
            $this->db = new Database;
        }

        public function getAllBands(){
                $this->db->query('SELECT * FROM bands
                                    ORDER BY band_name
                                ');
                $results = $this->db->resultSet();
            return $results;
        }
        
        public function getBandById($bandId){
            // $this->db->startTransaction();
            $this->db->query('SELECT * FROM bands WHERE band_id = (:band_id);');
            $this->db->bind(':band_id', $bandId);
            $bandAlreadyStored = $this->db->single();
            return $bandAlreadyStored;
        }

        public function add($data){
            // dump($data);
            // if($this->getBandByName($data['band_name'])){ return false; }
            $this->db->query('INSERT INTO bands (band_name) VALUES (:band_name)');
            $this->db->bind(':band_name', $data['band_name']);
            return $this->db->executeAndGetId();
        }


        public function getBandByName($bandName){
            $this->db->query('SELECT * FROM bands WHERE band_name = (:band_name);');
            $this->db->bind(':band_name', $bandName);
            return $this->db->single();
        }

        public function updateBandname($data){
            $this->db->query('UPDATE bands SET band_name = :band_name WHERE band_id = :band_id');
            $this->db->bind(':band_id', $data['band_id']);
            $this->db->bind(':band_name', $data['band_name']);
            return $this->db->execute();
        } 
    }