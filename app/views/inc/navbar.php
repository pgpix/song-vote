<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark mb-3">
    <div class="container justify-content-between">
        <a class="navbar-brand" href="<?php echo URLROOT; ?>"><?php echo SITENAME; ?></a>
        <?php if(isLoggedIn()) : ?>
            <div class="mx-lg-5 salut text-center" data-cuid="<?php echo $_SESSION['user_id'] ?>">
                Hi <?php echo $_SESSION['user_name']; ?>&emsp;
            </div> 
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsDefault" aria-controls="navbarsDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse flex-grow-0" id="navbarsDefault">
                <ul class="navbar-nav mr-auto flex-row justify-content-between flex-wrap">
                    <li class="nav-item text-center">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/charts">Charts</a>
                    </li>
                    <li class="nav-item text-center">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/charts/votesongs">Vote</a>
                    </li>
                    <li class="nav-item text-center">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/charts/add">Add</a>
                    </li>
                    <li class="nav-item text-center">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/bands">Bands</a>
                    </li>
                    <li class="nav-item text-center">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/pages/help">HELP!</a>
                    </li>
                    <li class="nav-item text-center">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/pages/about">WHY?</a>
                    </li>
                    <li class="nav-item text-center">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/users/logout"><i class="fas fa-door-open"></i>&emsp;Logout</a>
                    </li>
                </ul>
            </div>
        <?php else : ?>
            <div class="flex-grow-0">
                <ul class="navbar-nav mr-auto flex-row justify-content-between flex-wrap">
                    <li class="nav-item text-center">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/users/login"><i class="fas fa-door-open"></i>&emsp;Login</a>
                    </li>
                </ul>
            </div>
        <?php endif ?>
    </div>
</nav>