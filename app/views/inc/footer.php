        <div id="scrollTop" class="scroll-top opa-50 text-white bg-light-50">
            <i class='far fa-angle-double-up'></i>
        </div>
        <div id="hback" class="hback opa-50 text-white bg-light-50">
            <i class='far fa-angle-double-left'></i>
        </div>

    </div><!-- End Container -->

    <?php
        foreach (glob("js/*.js") as $js) {
            echo '<script src="'.URLROOT.'/'.$js.'"></script>'."\n\t";
        }
    ?>
</body>
</html>