<?php require APPROOT . '/views/inc/header.php';?>
    <?php flash('song_message'); ?>
    <div class="row">
        <div class="rankhead offset-1 col-10 p-0">
            <h1 class="text-white">Your Ranking</h1>
            <p class="sub text-white-50">Rank your songs by drag and drop</p>
        </div>
    </div>
    
    <div class="row">
        <?php if($data['unvotedInter']) : ?>
            <div id="unvoted" class="col-6 mb-5">
                <ul id="draggable" class="offset-1 col-10 p-0">
                    <?php foreach($data['unvotedInter'] as $key => $inter) :?>
                        <li class="row mb-3 notRated draggable" data-score="0" data-interid="<?php echo $inter->inter_id; ?>">
                            <div class="score handle col-4 py-2 pl-4 pr-1 btn d-flex justify-content-center align-items-center rounded-0 cursor-move text-primary bg-dark">
                                Not yet rated
                            </div>
                            <div class="col-8 text-center text-sm-left p-2 cursor-move interpretation">
                                <a href="<?php echo URLROOT; ?>/charts/details/<?php echo $inter->inter_id; ?>">
                                    <span class="text-capitalize text-white"><?php echo $inter->title; ?></span>
                                </a>
                                <br>
                                <span class="text-capitalize"><?php echo $inter->band_name; ?></span>
                            </div>
                        </li>
                    <?php endforeach;?>
                </ul>
            </div>
        <?php endif; ?>

        <?php if($data['unvotedInter']) : ?>
            <div class="scroll-wrap col-6">
        <?php else : ?>
            <div class="scroll-wrap col-12">
        <?php endif; ?>
                <ul id="sortable" class="sortable offset-1 col-10 p-0">
                    <?php if($data['votedInter']) : ?>
                        <?php foreach($data['votedInter'] as $key => $inter) :?>
                            <li class="row mb-3" data-score="<?php echo $inter->rating; ?>" data-interid="<?php echo $inter->inter_id; ?>">
                                <div class="score handle col-4 p-2 pl-4 btn rounded-0 cursor-move text-primary bg-dark">
                                    <br>points 
                                </div>
                                <div class="col-8 text-center text-sm-left p-2 pl-3 cursor-move bg-primary">
                                    <a href="<?php echo URLROOT; ?>/charts/details/<?php echo $inter->inter_id; ?>">
                                        <span class="text-capitalize text-white"><?php echo $inter->title; ?></span>
                                    </a>
                                    <br>
                                    <span class="text-capitalize"><?php echo $inter->band_name; ?></span>
                                    <?php addStarToggle($inter); ?>
                                </div>
                            </li>
                            <?php endforeach;?>
                    <?php else : ?>
                        <li class="row mb-3 dummy">
                            <p class="col-12 text-center p-2 pl-3 cursor-move bg-primary">
                                Drop your Items here!<br>
                                <i class='fas fa-angle-double-down pulser'></i>
                            </p>
                        </li>
                        <li class="row mb-3 dummy">
                            <p class="col-12 text-center p-2 pl-3 cursor-move bg-primary">
                                <i class='fas fa-angle-double-up pulser'></i>
                            </p>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
    </div>
<?php require APPROOT . '/views/inc/footer.php' ?>