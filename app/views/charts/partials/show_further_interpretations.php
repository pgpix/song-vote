<div class="row no-gutters p-3 my-3  bg-dark-70">
    <p class="col-6 mb-0 d-flex justify-content-center align-items-center text-primary">Also played by:</p>
    <div class="col-6">
        <?php foreach($data['furtherinter'] as $further) : ?>
            <p class="col-12 mb-0 d-flex justify-content-center text-capitalize text-muted" role="button">
                <a href="<?php echo URLROOT .'/bands/details/'.$further->band_id; ?>" class="int-link"><?php echo $further->band_name; ?></a>
            </p>
        <?php endforeach; ?>
    </div>
</div>