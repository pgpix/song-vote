<div class="row info-links no-gutters px-3 my-0"> 
    <h4 class="col-12 mb-2 d-flex justify-content-center align-items-center text-success">Links:</h4>
    <div class="row col-12 px-3">
    <?php foreach($data['urls'] as $url) : ?>
            <p class="position-relative col-12 col-sm-6 col-lg-3 mb-2 pl-4 d-flex align-items-center text-center text-muted" role="button">
                <a href="<?php echo $url->url; ?>" target="_blank" class="text-success"><?php echo $url->url_title; ?></a>&nbsp;<i class='fas fa-external-link-alt center-left small text-success'></i>
            </p>
        <?php endforeach; ?>
    </div>
</div>