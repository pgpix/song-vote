<form id="add-url" class="row px-3">
    <div class="col-12 col-lg-4 p-0 d-flex justify-content-center">
        <input type="text" id="url_title" name="url_title" class="form-control form-control-lg px-1 text-center text-success bg-dark-85" 
            placeholder="Enter an URL Title">
        <span class="invalid-feedback"></span>
    </div>
    <div class="col-12 col-lg-8 p-0 d-flex justify-content-center">
            <input type="text" name="url" class="form-control form-control-lg px-1 text-center text-success bg-dark-85" 
            placeholder="Enter an URL">
        <span class="invalid-feedback"></span>
        <input type="hidden" name="inter_id" value="<?php echo $data['interpretation']->inter_id; ?>">
        <button type="submit" value="addUrl" role="button" class="position-absolute bottom-right btn p-1 text-success bg-transparent pulser">
            <i class='far fa-pencil h5'></i>
        </button>
    </div>
</form>