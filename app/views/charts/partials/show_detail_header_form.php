<div class="col-12 col-lg-6 mb-4 m-lg-0 p-0 pr-lg-3 d-flex justify-content-center align-items-center text-white">
    <form id="send-title" class="col">
        <label for="title" class="position-absolute top-center small-label text-muted">Title:</label>
        <textarea type="text" id="title" name="title" rows="1" data-expandable
            class="form-control form-control-lg px-1 h1 text-center bg-light-50"
            value=""><?php echo $data['interpretation']->title; ?></textarea>
        <span class="invalid-feedback"></span>
        <input type="hidden" name="song_id"
            value="<?php echo $data['interpretation']->song_id; ?>">
        <input type="hidden" name="inter_id"
            value="<?php echo $data['interpretation']->inter_id; ?>">
        <button type="submit"
            value="Update"
            role="button"
            class="position-absolute bottom-right btn p-1 text-success bg-transparent pulser">
            <i class='far fa-pencil h5'></i>
        </button>
    </form>


</div>

<p class="col-12 col-lg-6 m-0 pl-lg-3 d-flex justify-content-center align-items-center text-capitalize">
    <span class="position-absolute top-center pl-lg-3 small-label">Band: </span>
    <a href="<?php echo URLROOT .'/bands/details/'.$data['interpretation']->band_id; ?>"
        class="int-link"><?php echo $data['interpretation']->band_name; ?></a>
</p>
<p class="col-12 d-flex justify-content-between m-0 text-sm text-capitalize text-muted">
    Added by <?php echo $data['user']->name; ?>
    <button type="submit"
        value="Delete"
        role="button"
        class="position-absolute bottom-right btn p-0 text-success bg-transparent pulser">
        <i class='fas fa-trash-alt' class="h5 p-0 m-0"></i>
    </button>
</p>