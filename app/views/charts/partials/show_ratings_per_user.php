<?php foreach($data['ratings'] as $rating) :?>
    <div class="d-flex mb-3">
        <div class="score col-6 p-3 btn rounded-0 text-success bg-dark-85">
            <?php echo $rating->rating ? $rating->rating : '0'; ?> pts
        </div>
        <div class="name col-6 p-3 text-sm-left text-capitalize text-white bg-success">
            <?php
                $starClass = 'star-lg text-gold ';
                if($rating->user_id == $_SESSION['user_id']){
                    $starClass .= 'star-toggle pulser ';
                }
                if($rating->liked) {
                    $starClass .= 'fas fa-star';
                } else {
                    $starClass .= 'far fa-star';
                }
                echo '<i class="'. $starClass .'" role="button" data-interid="'. $data['interpretation']->inter_id .'" data-cuid="'. $rating->user_id .'"></i>&emsp;';
                echo '<p class="d-inline text-dark">'.$rating->user_name.'</p>';
            ?>
        </div>
    </div>
<?php endforeach; ?>