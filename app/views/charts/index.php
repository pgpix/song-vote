<?php require APPROOT . '/views/inc/header.php' ;
    //dumpex($data);
    ?>
    <?php flash('song_message'); ?>
    <div class="row">
        <div class="offset-1 col-10 p-0 d-flex flex-wrap">
            <h1 class="col-12 col-md-6 p-0 text-center text-md-left">The Charts (<?php echo count($data['interpretations']);?>)</h1>
            <div id="starfilter" class="star-filter col-12 col-md-6 mb-3 p-1 d-flex justify-content-center align-items-center bg-dark-20 opa-85">
                <?php
                    generateStarFilter($data);
                ?>
            </div>
        </div>
    </div>

    <?php //dumpex($data['interpretations']); ?>
    <?php if($data['interpretations']) : ?>
        <div class="scroll-wrap">
            <ul id="totalscore" class="offset-1 col-10 p-0">
                <?php foreach($data['interpretations'] as $place => $inter) :?>
                    <li class="interpretation row mb-3<?php if($inter->score == 0) echo ' notRated ';?>" 
                        <?php 
                            echo 'data-interid='.$inter->inter_id.' ';
                        ?>>
                        <div class="total-score col-4 p-0 text-center align-middle rounded-0 text-primary bg-dark">
                            <div class="inline-block position-relative h1 m-0  w-50 text-center align-middle">
                                <?php echo $place + 1; ?>

                            </div>
                            <div class="inline-block w-25 py-3 small text-center align-middle">
                                <?php echo $inter->score == 0 ? 'Not yet rated' : $inter->score.'<br>pts'; ?>
                            </div>
                        </div>
                        
                        <div class="<?php echo $inter->score == 0 ? ' notRated interpretation' : ' bg-primary'; ?> col-8 p-2 pl-3 align-middle">
                            <a href="<?php echo URLROOT; ?>/charts/details/<?php echo $inter->inter_id; ?>">
                                <span class="text-capitalize text-white"><?php echo $inter->title; ?></span>
                            </a>
                            <br>
                            <a href="<?php echo URLROOT; ?>/bands/details/<?php echo $inter->band_id; ?>">
                                <span class="text-capitalize text-dark"><?php echo $inter->band_name; ?></span>
                            </a>
                            <?php addStarToggle($inter); ?>
                        </div>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
    <?php endif; ?>
<?php require APPROOT . '/views/inc/footer.php' ?>