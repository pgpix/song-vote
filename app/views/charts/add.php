<?php require APPROOT . '/views/inc/header.php' ?>
    <?php flash('song_message'); ?>
    <div class="row mb-3">
        <div class="offset-1 col-10 p-0">
            <h1>Add A Song</h1>
        </div>
    </div>
    <?php //dump($data['songs']);  ?>
    <div class="row mt-3">
        <div class="offset-1 col-10 p-0">
            <form action="<?php echo URLROOT; ?>/charts/add" method="post">
                <div class="form-group d-flex position-relative">
                    <label for="title" class="col-4 p-2 my-0 h5 text-center rounded-0 text-primary bg-dark">Title: <sup>*</sup></label>
                    <input type="text" id="title" name="title" autofocus class="form-control form-control-lg col-8 rounded-0 border-0 bg-primary text-white <?php echo (!empty($data['title_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['title']; ?>">
                    <span class="invalid-feedback"><?php echo $data['title_err']; ?></span>
                </div>
                <div class="form-group d-flex">
                    <label for="band_name" class="col-4 p-2 my-0 h5 text-center rounded-0 text-primary bg-dark">Band: </label>
                    <input type="text" id="band_name" name="band_name" class="form-control form-control-lg col-8 rounded-0 border-0 bg-primary text-white">
                </div>
                <input type="submit" value="Submit" formmethod="post" class="btn btn-success">
            </form>
        </div>
    </div>

<?php require APPROOT . '/views/inc/footer.php' ?>