<?php require APPROOT . '/views/inc/header.php'; ?>

    <div class="row no-gutters text-capitalize px-2 pt-4 pb-0 my-3 bg-warning">
        <?php require_once APPROOT . '/views/charts/partials/show_detail_header_form.php'; ?>
    </div>

    <?php if($data['furtherinter']) : ?>
        <?php require_once APPROOT . '/views/charts/partials/show_further_interpretations.php'; ?>
    <?php endif; ?>

    <div class="no-gutters p-3 my-3 bg-dark-85">
        <?php if($data['urls']) require_once APPROOT . '/views/charts/partials/show_urls.php'; ?>
        <?php require_once APPROOT . '/views/charts/partials/show_url_form.php'; ?>
    </div>

    <div class="ratings">
        <?php require_once APPROOT . '/views/charts/partials/show_ratings_per_user.php';?>
    </div>

<?php require APPROOT . '/views/inc/footer.php' ?>