<?php require APPROOT . '/views/inc/header.php' ?>

    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-dark mt-5 br-2">
                <?php flash('register_success'); ?>
                <h2>Login</h2>
                <p>Please fill out this form with your E-Mail and Password to login</p>
                <form action="<?php echo URLROOT; ?>/users/login" method="post">
                    <div class="form-group">
                        <label for="email">E-Mail: <sup>*</sup></label>
                        <input type="text" id="email" name="email" class="form-control form-control-lg <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['email']; ?>">
                        <span class="invalid-feedback"><?php echo $data['email_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="password">Password: <sup>*</sup></label>
                        <input type="password" id="password" name="password" class="form-control form-control-lg <?php echo (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['password']; ?>">
                        <span class="invalid-feedback"><?php echo $data['password_err']; ?></span>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input type="submit" value="Login" class="btn btn-success btn-block">
                        </div>
                        <div class="col">
                        <?php if(NO_NEW_USERS_ALLOWED) : ?>
                            For a new user registration ask your admin please.
                        <?php else : ?>
                            <a href="<?php echo URLROOT ?>/users/register" class="btn btn-light btn-block">Have no account? Please register here.</a>
                        <?php endif ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="d-flex align-items-center justify-content-between">
        <a href="<?php echo URLROOT; ?>/pages/help" class="btn btn-primary my-5"> HELP </a>
        <a href="<?php echo URLROOT; ?>/pages/about" class="btn btn-primary"> Why? </a>
    </div>

<?php require APPROOT . '/views/inc/footer.php' ?>