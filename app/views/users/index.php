<?php require APPROOT . '/views/inc/header.php' ?>
<div class="row mb-3">
    <div class="offset-1 col-10 p-0">
        <h1>Registrierte Benutzer (<?php echo count($data['users']);?>)</h1>
    </div>
</div>

    <?php if($data['users']) : ?>
        <ol class="offset-1 col-10 p-0">
            <?php foreach($data['users'] as $user) :?>
                <li class="row mb-3">
                    <div class="col-4 p-2 btn rounded-0 text-primary bg-dark">
                        <?php echo $user->name; ?> 
                    </div>
                    <div class="col-8 text-left text-sm-left p-2 pl-3 bg-primary">
                        <span class="text-white"><?php echo $user->email; ?></span>
                    </div>
                </li>
            <?php endforeach;?>
        </ol>
    <?php endif; ?>


<?php require APPROOT . '/views/inc/footer.php' ?>