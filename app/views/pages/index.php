<?php require APPROOT . '/views/inc/header.php' ?>

<div class="col-12 p-3 mb-3 text-center bg-dark opa-85">
    <p class="d-flex justify-content-between p-1">
        <a href="<?php echo URLROOT; ?>/pages/help" class="btn btn-primary"> HELP </a>
        <a href="<?php echo URLROOT; ?>/pages/about" class="btn btn-primary"> ABOUT </a>
    </p>
    <h1><?php echo $data['title'] ?></h1>
    <p class="lead p-1"><?php echo $data['description'] ?></p>
</div>

<?php if($data['interpretations']) : ?>
        <div class="scroll-wrap">
            <ul id="totalscore" class="offset-1 col-10 p-0">
                <?php foreach($data['interpretations'] as $place => $inter) :?>
                    <li class="interpretation row mb-3<?php if($inter->score == 0) echo ' notRated ';?>" 
                        <?php 
                            echo 'data-interid='.$inter->inter_id.' ';
                        ?>>
                        <div class="total-score col-4 p-0 text-center align-middle rounded-0 text-primary bg-dark">
                            <div class="inline-block position-relative h1 m-0  w-50 text-center align-middle">
                                <?php echo $place + 1; ?>

                            </div>
                            <div class="inline-block w-25 py-3 small text-center align-middle">
                                <?php echo $inter->score == 0 ? 'Not yet rated' : $inter->score.'<br>pts'; ?>
                            </div>
                        </div>
                        
                        <div class="<?php echo $inter->score == 0 ? ' notRated interpretation' : ' bg-primary'; ?> col-8 p-2 pl-3 align-middle">
                            <a href="<?php echo URLROOT; ?>/charts/details/<?php echo $inter->inter_id; ?>">
                                <span class="text-capitalize text-white"><?php echo $inter->title; ?></span>
                            </a>
                            <br>
                            <a href="<?php echo URLROOT; ?>/bands/details/<?php echo $inter->band_id; ?>">
                                <span class="text-capitalize text-dark"><?php echo $inter->band_name; ?></span>
                            </a>
                        </div>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
    <?php endif; ?>
<?php require APPROOT . '/views/inc/footer.php' ?>