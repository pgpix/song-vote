<?php require APPROOT . '/views/inc/header.php' ?>

<div class="col-12 offset-lg-2 col-lg-8 mt-5 mb-2 p-4 text-center bg-dark opa-85">
    <div class="container">         
        <h1><?php echo $data['title'] ?></h1>
        <p class="lead"><?php echo $data['description'] ?></p>
    </div>
</div>


<div class="col-12 offset-lg-2 col-lg-8 d-flex flex-column align-items-center p-4 bg-dark opa-85">
        <h3>At first</h3>
        <div class="col-12 p-3 bg-black opa-85">
            <h4 class="text-center">I wanted to practice</h4>
            <ul>
                <li>AJAX</li>
                <li>bootstrap</li>
                <li>MySQL and database normalization</li>
                <li>object oriented PHP including PDO</li>
                <li>jQuery UI to vote via drag and drop</li>
                <li>touchpunch to make jQuery UI working on touchdevices</li>
                <li>and last but not least I learned the Model View Controller Pattern in depth</li>
            </ul>
            <p class="text-center">View source at <a href="https://gitlab.com/pgpix/song-vote" target="_blank" class="text-gold">GitLab</a>.</p>
        </div>

    <hr class="separator">

    <h3>At second</h3>
    <div class="col-12 p-3 bg-black opa-85">
        <p class="m-2 text-center">I tried to get rid of the discussion what to play at the next rehearsal <br><i class='far fa-smile-wink m-2 text-xl text-gold'></i></p>
    </div>

</div>

<?php require APPROOT . '/views/inc/footer.php' ?>