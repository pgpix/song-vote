<?php require APPROOT . '/views/inc/header.php' ?>

<div class="col-12 offset-lg-2 col-lg-8 mt-5 mb-2 p-4 text-center bg-dark opa-85">
    <h1><?php echo $data['title'] ?></h1>
    <p class="lead"><?php echo $data['description'] ?></p>
</div>
<div class="col-12 offset-lg-2 col-lg-8 d-flex flex-column align-items-center mb-2 p-4 bg-dark opa-85">
    <h2 class="text-center">Where does the navigation lead me?</h2>
    <div class="col-12">
        <hr class="separator">
        <h3 class="text-center">"Charts"</h3>
        <p class="mb-4 text-center">
            displays the common hitlist.
        </p>
        <hr class="separator">
        <h3 class="text-center">"Vote"</h3> 
        <p class=" text-center">
            is the page where the magic lives. Here you can sort your favorites via drag and drop. The dark areas on the left are the handles to move the items. 
        </p>
        <hr class="separator">
        <h3 class="text-center">"Add"</h3> 
        <p class=" text-center">
            is the page where you can add a new song including a band. 
        </p>
    </div>
</div>
<div class="col-12 offset-lg-2 col-lg-8 d-flex flex-column align-items-center mb-2 p-4 bg-dark opa-85">
    <h2>Features</h2>
    <p>(not that obvious)</p>
    <div class="col-12">
        <h3 class="text-center">"Star-Filter"</h3> 
        <p class="mb-5 text-center">
            With the grey stars on the right you can select if you want to play the song at the next gig, rehearsal or at all. 
            By clicking on Star above, you filter the song chosen by this member. 
            You will get an intersection of all songs that are "liked" by the activated members
            <br><br><img src="<?php echo URLROOT; ?>/img/star-toggles.png" class="mw-90"  alt="feature explanation">
        </p>
    </div>
    <div class="col-12">
        <h3 class="text-center">"Detail Links"</h3> 
        <p class="mb-5 text-center">
            Clicking on a songtitle or a bandname leads you to a detailspage about it
            <br><br><img src="<?php echo URLROOT; ?>/img/link-to-details.png" class="mw-90"  alt="feature explanation">
        </p>
    </div>
    <div class="col-12">
        <h3 class="text-center">"Detail View"</h3> 
        <p class="mb-5 text-center">
            The detailspage not only shows the details, but lets you add links to additional informations, resources, streamingplattforms or videos.
            <br><br><img src="<?php echo URLROOT; ?>/img/details-view.png" class="mw-90"  alt="feature explanation">
        </p>
    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php' ?>