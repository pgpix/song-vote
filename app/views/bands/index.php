<?php require APPROOT . '/views/inc/header.php'; ?> 
<?php //dumpex($data);?>
    <?php flash('song_message'); ?>
    <div class="row mb-3">
        <div class="offset-1 col-10 p-0">
            <h1>Bands (<?php echo count($data['bands']);?>)</h1>
        </div>
    </div>

    <?php if($data['bands']) : ?>
        <ul id="bands" class="row list-unstyled p-0">
            <?php foreach($data['bands'] as $place => $band) :?>
                <li class="col-12 col-sm-6 col-lg-3 mb-3">
                    <div class="text-center text-sm-left p-2 pl-3 align-middle bg-primary">
                        <a href="<?php echo URLROOT; ?>/bands/details/<?php echo $band->band_id; ?>" class="text-white">
                            <?php echo $band->band_name;?>
                        </a>
                    </div>
                </li>
            <?php endforeach;?>
        </ul>
    <?php endif; ?>
<?php require APPROOT . '/views/inc/footer.php' ?>