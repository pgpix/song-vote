<?php require APPROOT . '/views/inc/header.php' ?>
<?php  //dumpex($data); ?>
    <div class="row no-gutters text-capitalize px-2 pt-4 pb-0 my-3  bg-warning">
        <h1 class="col-12 mb-4 m-lg-0 p-0 pr-lg-3 d-flex justify-content-center align-items-center text-white">
            <form action="<?php echo URLROOT; ?>/bands/details/<?php echo $data["band_id"]; ?>" method="post" class="col mb-3">
                <label for="band_name" class="position-absolute px-1 top-center small-label text-muted">Band:</label>
                <textarea data-expandable 
                    class="form-control h1 px-1 text-center bg-light-50 <?php echo (!empty($data['band_name_err'])) ? 'is-invalid' : ''; ?>"
                    type="text" name="band_name" rows="1"><?php echo $data['band_name']; ?></textarea>

                <span class="invalid-feedback small"><?php echo $data['band_name_err']; ?></span>
                <button type="submit" value="Update" role="button" class="position-absolute bottom-right btn p-1 text-success bg-transparent">
                    <i class='far fa-pencil h5'></i>
                </button>
            </form>
        </h1>
    </div>
    <?php if($data['band_songs']) : ?>
        <div class="row no-gutters p-3 my-3 bg-success">    
            <p class="col-12 h2 mb-3 d-flex justify-content-center align-items-center text-dark">Song(s):</p>
            <div class="col-12">
                <?php foreach($data['band_songs'] as $song) : ?>
                    <p class="col-12 mb-2 d-flex justify-content-center align-items-center text-capitalize" role="button">
                        <a href="<?php echo URLROOT.'/charts/details/'.$song->inter_id; ?>" class="mb-0 h4 int-link text-dark">
                        <?php echo $song->title;?></a>
                    </p>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
<?php require APPROOT . '/views/inc/footer.php' ?>