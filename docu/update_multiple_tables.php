   public function registerFreelancer($firstname, $lastname, $email, $password, $location, $portfolio, $jobtitle, $priceperhour, $experience, $bio, $userType){

        global $bcrypt;
        global $mail;

        $time       = time();
        $ip         = $_SERVER['REMOTE_ADDR'];
        $email_code = sha1($email + microtime());
        $password   = $bcrypt->genHash($password);// generating a hash using the $bcrypt object

        $query  = $this->db->prepare("INSERT INTO " . DB_NAME . ".users
            (firstname, lastname, email, email_code, password, time_joined, location, portfolio, bio, ip) 
            VALUES 
            (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        ");

        $query->bindValue(1, $firstname);
        $query->bindValue(2, $lastname);
        $query->bindValue(3, $email);
        $query->bindValue(4, $email_code);
        $query->bindValue(5, $password);
        $query->bindValue(6, $time);
        $query->bindValue(7, $location);
        $query->bindValue(8, $portfolio);
        $query->bindValue(9, $bio);
        $query->bindValue(10, $ip);

        try{
            $query->execute();

            // Send email code usually here

            $rows = $query->rowCount();

            if($rows > 0){

                $last_user_id =  $this->db->lastInsertId('user_id');

                $query_2 = $this->db->prepare("INSERT INTO " . DB_NAME . ".freelancers (freelancer_id, jobtitle, priceperhour) VALUE (?,?,?)");

                $query_2->bindValue(1, $last_user_id);
                $query_2->bindValue(2, $jobtitle);
                $query_2->bindValue(3, $priceperhour);                      

                $query_2->execute();

                $query_3 = $this->db->prepare("INSERT INTO " . DB_NAME . ".user_types (user_type_id, user_type) VALUE (?,?)");

                $query_3->bindValue(1, $last_user_id);
                $query_3->bindValue(2, $userType);              

                $query_3->execute();

                $query_4 = $this->db->prepare("INSERT INTO " . DB_NAME . ".user_experience (experience_id, experience) VALUE (?,?)");

                $query_4->bindValue(1, $last_user_id);
                $query_4->bindValue(2, $experience);                            

                $query_4->execute();

                if($userType == 'designer') {
                    $query_5 = $this->db->prepare("INSERT INTO " . DB_NAME . ".designer_titles (job_title_id, job_title) VALUE (?,?)");

                    $query_5->bindValue(1, $last_user_id);
                    $query_5->bindValue(2, $jobtitle);              

                    $query_5->execute();

                } else if ($userType == 'developer') {
                    $query_5 = $this->db->prepare("INSERT INTO " . DB_NAME . ".developer_titles (job_title_id, job_title) VALUE (?,?)");

                    $query_5->bindValue(1, $last_user_id);
                    $query_5->bindValue(2, $jobtitle);              

                    $query_5->execute();
                }

                return true;
            }

        }catch(PDOException $e){
            die($e->getMessage());
        }   
    }