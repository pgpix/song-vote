const path = require('path');
var webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: {
        index: './src/index.js',
        details: './src/detailsview.js',
        stars: './src/like-stars.js'
    },
    output: {
        path: path.resolve(__dirname, 'public/js'),
        filename: '[name].[contenthash].js',
        clean: true
    },
    devtool: "source-map",
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
           }),
        new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: [
            'main*'
        ],}),
    ],
};